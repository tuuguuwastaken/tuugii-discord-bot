import openai as oa
from dotenv import load_dotenv
import os
load_dotenv()

oa.api_key = os.getenv("OPENAI_TOKEN")
oa.Model.list()

completion = oa.ChatCompletion.create(
model="gpt-3.5-turbo",
    messages=[
        {"role": "user", "content": "Tell the world about the ChatGPT API in the style of a pirate."}
    ]
)

print(completion.choices[0].message.content)